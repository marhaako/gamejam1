module GameJam1 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires javafx.swing;
    requires javafx.media;

    opens GameJam1 to javafx.fxml;
    exports GameJam1;
}