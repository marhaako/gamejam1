package GameJam1;

import java.util.TimerTask;

public class TickPlayer extends TimerTask {

    Player player;
    Boolean animated = false;

    TickPlayer(Player player) {
        this.player = player;
    }
    public void run() {
        if(animated) {
            player.setHeight(player.getHeight()-5);
            animated = false;
        } else {
            player.setHeight(player.getHeight()+5);
            animated = true;
        }
    }
}
