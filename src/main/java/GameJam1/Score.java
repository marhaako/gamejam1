package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Score extends Rectangle {

    int score = -1;

    Score() {
        super(100,200);
        setTranslateX(0);
        setTranslateY(20);
        update();
    }

    public void update() {
        score++;
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/scores/" + score + ".png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
