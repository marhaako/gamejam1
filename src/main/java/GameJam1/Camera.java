package GameJam1;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class Camera {

    Camera() {

    }

    public void moveCameraUp(Pane pane) {
        Node n = pane.getChildren().get(0);
        if(n.getTranslateY() < 0) {
            for (Node node : pane.getChildren()) {
                if(node instanceof Tile || node instanceof Objective || node instanceof ObjectiveBox || node instanceof Player) {
                    node.setTranslateY(node.getTranslateY() + App.MOVESIZE);
                }
            }
        }

    }

    public void moveCameraDown(Pane pane) {
        Node n = pane.getChildren().get(0);
        if(n.getTranslateY() >  -(App.HEIGHTBLOCKS * 100) + App.HEIGHT)
        for (Node node : pane.getChildren()) {
            if(node instanceof Tile || node instanceof Objective || node instanceof ObjectiveBox || node instanceof Player) {
                node.setTranslateY(node.getTranslateY() - App.MOVESIZE);
            }
        }
    }

    public void moveCameraRight(Pane pane) {
        Node n = pane.getChildren().get(0);
        if(n.getTranslateX() >  -(App.WIDTHBLOCKS * 100) + App.WIDTH)
            for (Node node : pane.getChildren()) {
                if(node instanceof Tile || node instanceof Objective || node instanceof ObjectiveBox || node instanceof Player) {
                    node.setTranslateX(node.getTranslateX() - App.MOVESIZE);
                }
        }
    }

    public void moveCameraLeft(Pane pane) {
        Node n = pane.getChildren().get(0);
        if(n.getTranslateX() <  0)
        for (Node node : pane.getChildren()) {
            if(node instanceof Tile || node instanceof Objective || node instanceof ObjectiveBox || node instanceof Player) {
                node.setTranslateX(node.getTranslateX() + App.MOVESIZE);
            }
        }
    }
}
