package GameJam1;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.Timer;

/**
 * JavaFX App
 */
public class App extends Application {

    public static int WIDTH = 1800;
    public static int HEIGHT = 1000;
    public static int PLAYERHEIGHT = 100;
    public static int PLAYERWIDTH = 100;
    public static int MOVESIZE = 100;
    public static int WIDTHBLOCKS = 61;
    public static int HEIGHTBLOCKS = 35;


    private static Pane pane;
    Camera camera = new Camera();
    Player player = new Player(300, 350, PLAYERWIDTH, PLAYERHEIGHT);
    Objective objective = null;
    ObjectiveBox objectiveBox = null;
    Score score = new Score();

    private Parent createContent() {
        Timer timer = new Timer();
        timer.schedule(new TickPlayer(player), 0, 500);
        MapBuilder mb = new MapBuilder();
        pane = mb.getMap();
        playSound("/sounds/music.mp3", 100);
        objectiveBox = new ObjectiveBox(900,100,PLAYERWIDTH, PLAYERHEIGHT, pane);
        objective = objectiveBox.newObjective(pane);
        pane.getChildren().add(objective);
        pane.getChildren().add(objectiveBox);
        pane.getChildren().add(player);
        timer.schedule(new TickCountDown(pane), 0, 10000);
        pane.getChildren().add(score);
        timer.schedule(new WelcomeScreen(pane), 0, 5000);


        return pane;
    }

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(createContent());
        stage.setScene(scene);
        scene.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case LEFT:
                case A:
                    if(standable("left")) {
                        player.moveLeft();
                        if (player.getTranslateX() < 400) {
                            camera.moveCameraLeft(pane);
                        }
                    }
                    break;
                case RIGHT:
                case D:
                    if(standable("right")) {
                        player.moveRight();
                        if (player.getTranslateX() > WIDTH - 500) {
                            camera.moveCameraRight(pane);
                        }
                    }
                    break;
                case UP:
                case W:
                    if(standable("up")) {
                        player.moveUp();
                        if (player.getTranslateY() < 400) {
                            camera.moveCameraUp(pane);
                        }
                    }
                    break;
                case DOWN:
                case S:
                    if(standable("down")) {
                        player.moveDown();
                        if (player.getTranslateY() > HEIGHT - 500) {
                            camera.moveCameraDown(pane);
                        }
                    }
                    break;
                case E:
                    checkObjective();
                    newObjective();
                    break;
            }
        });
        stage.show();
    }

    public Boolean standable(String direction) {
        Boolean standable = false;
        double playerHeight = player.getTranslateY() + 50;
        double playerWidth = player.getTranslateX();

        switch(direction) { //Calculate the players location after moving
            case "up":
                playerHeight -= 100;
                break;
            case "down":
                playerHeight += 100;
                break;
            case "right":
                playerWidth += 100;
                break;
            case "left":
                playerWidth -= 100;
                break;
        }

        for(Node n : pane.getChildren()) {
            if(n instanceof Tile) {
                Tile m = (Tile) n;
                double tileHeight = m.getTranslateY();
                double tileWidth = m.getTranslateX();
                if(tileHeight == playerHeight && tileWidth == playerWidth) { //Finds the tile player wants to move to
                    {
                        if(m.standable()) { //If the tile is standable, set standable to true
                            standable = true;
                        }
                    }
                }
            }
        }
        return standable;
    }

    public void checkObjective() {
        Rectangle r3 = player.getBounds();
        Rectangle r4 = objective.getBounds();
        if(r3.intersects(r4)) {
            System.out.println("YES");
            objective.setHeight(0);
            objective.setWidth(0);
            score.update();
            objectiveBox.hide();
        }
    }

    public void newObjective() {
        Rectangle r3 = player.getBounds();
        Rectangle r4 = objectiveBox.getBounds();
        if(r3.intersects(r4)) { //Check if user wants new objective from Objective Box
            System.out.println("YES");
            if(objective.getHeight() == 0) { //If current objective is done
                playSound("/sounds/newobj.wav", 10);
                objective = objectiveBox.newObjective(pane);    //Create new objective
                pane.getChildren().add(objective);
            }
        }
    }

    public void playSound(String sound, int vol){
        URL file = getClass().getResource(sound);
        Media media = new Media(file.toString());
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setVolume(vol);
        mediaPlayer.play();
    }


    public static void main(String[] args) {
        launch();

    }


}