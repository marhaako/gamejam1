package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;


public class Tile extends Rectangle {

    Boolean stand;

    Tile(int x, int y, int w, int h, String type) {
        super(w, h);
        setTranslateX(x);
        setTranslateY(y);
        switch(type) {
            case "grass":
            case "sand":
            case "dirt":
                stand = true;
                break;
            case "water":
                stand = false;
                break;
        }
        try {
            BufferedImage img = null;
            if(type.equals("grass")) {
                img = ImageIO.read(getClass().getResource("/images/grass.png"));
            } else if(type.equals("water")) {
                img = ImageIO.read(getClass().getResource("/images/water.png"));
            } else if(type.equals("dirt")) {
                img = ImageIO.read(getClass().getResource("/images/dirt.png"));
            } else if(type.equals("sand")) {
                img = ImageIO.read(getClass().getResource("/images/sand.png"));
            }
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    Boolean standable() {
        return stand;
    }
}
