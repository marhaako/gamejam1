package GameJam1;

import javafx.scene.layout.Pane;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class MapBuilder {

    Pane pane;


    MapBuilder() {
        pane = new Pane();
        pane.setPrefSize(App.WIDTH, App.HEIGHT);
        File file = new File(getClass().getResource("/maps/map1.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            int tileType;
            int xCount = 0;
            int yCount = 0;
            while ((tileType = br.read()) != -1) {
                switch ((char)tileType) {
                    case '0':
                        pane.getChildren().add(new Tile(xCount * 100, yCount * 100, App.PLAYERWIDTH, App.PLAYERHEIGHT, "grass"));
                        xCount++;
                        break;
                    case '1':
                        pane.getChildren().add(new Tile(xCount * 100, yCount * 100, App.PLAYERWIDTH, App.PLAYERHEIGHT, "water"));
                        xCount++;
                        break;
                    case '2':
                        pane.getChildren().add(new Tile(xCount * 100, yCount * 100, App.PLAYERWIDTH, App.PLAYERHEIGHT, "dirt"));
                        xCount++;
                        break;
                    case '3':
                        pane.getChildren().add(new Tile(xCount * 100, yCount * 100, App.PLAYERWIDTH, App.PLAYERHEIGHT, "sand"));
                        xCount++;
                        break;
                }
                if(xCount == App.WIDTHBLOCKS) {
                    xCount = 0;
                    yCount++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Rectangle death = new Rectangle();
            BufferedImage img = null;
            img = ImageIO.read(getClass().getResource("/images/grass.png"));


        } catch(IOException e) {
            e.printStackTrace();
        }

    }

    public Pane getMap() {
        return pane;
    }
}
