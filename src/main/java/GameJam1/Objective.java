package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class Objective  extends Rectangle {
    Pane pane;

    Objective(Pane pane, String objName) {
        super(100, 100);
        this.pane = pane;
        Random rand = new Random();
        boolean valid = false;
        int x = 0;
        int y = 0;
        do {
            x = rand.nextInt(App.WIDTHBLOCKS) * App.PLAYERWIDTH;
            y = rand.nextInt(App.HEIGHTBLOCKS) * App.PLAYERHEIGHT;
            valid = validSpawn(x, y);
        } while(!valid);
        setTranslateX(x);
        setTranslateY(y);
        BufferedImage img = null;
        System.out.println(objName);

        try {
            switch(objName) {
                case "wife":
                    img = ImageIO.read(getClass().getResource("/images/wife.png"));
                    this.setHeight(200);
                    break;
                case "xanax":
                    img = ImageIO.read(getClass().getResource("/images/xanax.png"));
                    this.setHeight(100);
                    break;
                case "car":
                    img = ImageIO.read(getClass().getResource("/images/car.png"));
                    this.setWidth(200);
                    break;
                case "cash":
                    img = ImageIO.read(getClass().getResource("/images/cash.png"));
                    break;
                case "balloon":
                    img = ImageIO.read(getClass().getResource("/images/balloon.png"));
                    this.setHeight(200);
                    this.setWidth(200);
                    break;
                case "college":
                    img = ImageIO.read(getClass().getResource("/images/college.png"));
                    break;
                case "mc":
                    img = ImageIO.read(getClass().getResource("/images/mc.png"));
                    this.setWidth(200);
                    break;
                case "kanye":
                    img = ImageIO.read(getClass().getResource("/images/kanye.png"));
                    break;
                case "bananasplit":
                    img = ImageIO.read(getClass().getResource("/images/bananasplit.png"));
                    this.setWidth(200);
                    this.setHeight(200);
                    break;
            }
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public java.awt.Rectangle getBounds() {
        return new java.awt.Rectangle((int)this.getTranslateX(), (int)this.getTranslateY(), (int)this.getWidth(), (int)this.getHeight());
    }

    public Boolean validSpawn(int x, int y) {
        Boolean valid = false;
        for(Node n : pane.getChildren()) {
            if(n instanceof Tile) {
                Tile m = (Tile) n;
                double tileHeight = m.getTranslateY();
                double tileWidth = m.getTranslateX();
                if(tileHeight == y && tileWidth == x) { //Find the tile we want to place the objective on
                    {
                        if(m.standable()) { //If the tile is valid for an objective
                            valid = true;
                        }
                    }
                }
            }
        }
        return valid;
    }
}
