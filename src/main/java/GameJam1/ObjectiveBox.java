package GameJam1;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ObjectiveBox  extends Rectangle {

    String[] objectives = {"wife", "xanax", "car", "cash", "balloon", "college", "mc", "kanye", "bananasplit"};
    int objectiveNo = -1;
    ObjectiveText objText = new ObjectiveText(0, 250, 300, 100);

    ObjectiveBox(int x, int y, int w, int h, Pane pane) {
        super(w, h);
        List<String> list = Arrays.asList(objectives);
        Collections.shuffle(list);
        objectives = list.toArray(new String[objectives.length]);
        setTranslateX(x);
        setTranslateY(y);
        setFill(Color.AQUA);
        pane.getChildren().add(objText);
    }

    public java.awt.Rectangle getBounds() {
        return new java.awt.Rectangle((int)this.getTranslateX(), (int)this.getTranslateY(), (int)this.getWidth(), (int)this.getHeight());
    }

    public Objective newObjective(Pane pane) {
        objectiveNo++;
        if(objectiveNo > objectives.length -1) {
            objectiveNo = 0;
        }
        objText.setHeight(100);
        objText.setText(objectives[objectiveNo]);
        return new Objective(pane, objectives[objectiveNo]);
    }

    public void hide() {
        objText.setHeight(0);
    }
}
