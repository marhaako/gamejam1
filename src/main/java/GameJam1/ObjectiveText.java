package GameJam1;


import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ObjectiveText extends Rectangle {
    ObjectiveText(int x, int y, int w, int h) {
        super(w,h);
        setTranslateX(x);
        setTranslateY(y);
    }

    public void setText(String type) {
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/objectiveText/" + type + "text.png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
