package GameJam1;

import javafx.scene.layout.Pane;

import java.util.TimerTask;

public class TickCountDown extends TimerTask {
    private static int noOfBars = 21;
    CountDownBlock[] bars = new CountDownBlock[noOfBars];

    Pane pane;

    TickCountDown(Pane pane) {
        this.pane = pane;
        for(int i = 0; i < noOfBars; i++) {
            bars[i] = new CountDownBlock(200 + i*50, 20, 50, 50);
            pane.getChildren().add(bars[i]);
        }
    }

    public void run() {
        if(noOfBars == 0) {

        } else {
            noOfBars--;
            System.out.println(noOfBars);
            bars[noOfBars].setHeight(0);
            bars[noOfBars].setWidth(0);
        }
    }
}
