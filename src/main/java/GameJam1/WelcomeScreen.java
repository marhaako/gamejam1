package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.TimerTask;

public class WelcomeScreen extends TimerTask {

    Screen screen = null;
    int p = 0;

    WelcomeScreen(Pane pane) {
        screen = new Screen(pane);
        pane.getChildren().add(screen);
    }
    public void run() {
        p++;
        if(p > 1) {
            screen.setHeight(0);
        }
    }


    private class Screen extends Rectangle {

        Screen(Pane pane) {
            super(App.WIDTH, App.HEIGHT);
            setTranslateX(0);
            setTranslateY(0);
            try {
                BufferedImage img = ImageIO.read(getClass().getResource("/images/start/welcome.png"));
                setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }
}
