package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class CountDownBlock extends Rectangle {

    CountDownBlock(int x, int y, int w, int h) {
        super(w, h);
        setTranslateX(x);
        setTranslateY(y);
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/redblock.png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
