package GameJam1;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Player extends Rectangle {




    Player(int x, int y, int w, int h) {
        super(w, h);
        setTranslateX(x);
        setTranslateY(y);
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/player_right.png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    void moveLeft() {
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/player_left.png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(getTranslateX() >= 0 + App.MOVESIZE) {
            setTranslateX(getTranslateX() - App.MOVESIZE);
        }
    }

    void moveRight() {
        try {
            BufferedImage img = ImageIO.read(getClass().getResource("/images/player_right.png"));
            setFill(new ImagePattern(SwingFXUtils.toFXImage(img, null)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(getTranslateX() < App.WIDTH - App.PLAYERWIDTH) {
            setTranslateX(getTranslateX() + App.MOVESIZE);
        }
    }

    void moveUp() {
        if(getTranslateY() - App.MOVESIZE + 50 >= 0) {
            setTranslateY(getTranslateY() - App.MOVESIZE);
        }
    }

    void moveDown() {
        if(getTranslateY() + App.MOVESIZE <= App.HEIGHT - App.PLAYERHEIGHT*1.25) {
            setTranslateY(getTranslateY() + App.MOVESIZE);
        }
    }

    public java.awt.Rectangle getBounds() {
        return new java.awt.Rectangle((int)this.getTranslateX(), (int)this.getTranslateY(), (int)this.getWidth(), (int)this.getHeight());
    }


}
