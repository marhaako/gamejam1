# The Bucket List
Author: Marius Håkonsen, https://bitbucket.org/marhaako/
## About
This game was the result of my participation in the WeeklyGameJam https://itch.io/jam/weekly-game-jam-115 .

My goal for the development of this game was to learn more about Java and JavaFX.

## How to play
Use the arrows or WASD to move around the map. 

Press E to interact with the different objectives scattered around the map. If your character intersects with the object, it disappears 
and you have to go back to the Bucket List to get a new object. When your character intersects with  the Bucket List, press E, and a new objective
will spawn somewhere on the map. 

The goal is to do as many things on the bucket list as you can before the time runs out, resulting in your instant death.

